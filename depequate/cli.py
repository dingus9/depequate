
import argparse
import json

from depequate.providers import all_providers


output = 'Not initialized'


def split_args(args):

    return args.split(',')


def build_args():

    main_parser = argparse.ArgumentParser('depequate')

    main_parser.add_argument('--output',
                             choices=('json', 'text', 'yaml'),
                             default='text',
                             help='Set the output format to one of %(choices)s')

    subs = main_parser.add_subparsers(dest='command')

    list = subs.add_parser('list',
                           help='List something %(prog)s knows about')
    provides = subs.add_parser('provides',
                               help='Search for packages that '
                                    'provide a library')
    equiv = subs.add_parser('equiv',
                            help='Find an equivalent package in '
                                 'other repo(s)')

    list.add_argument('what',
                      choices=('providers', 'repos', 'datbases'),
                      help='List something %(prog)s knows about: %(choices)s')

    provides.add_argument('name',
                          help='Library name')

    provides.add_argument('--repos',
                          type=split_args,
                          help='Limit to specific repositories comma , sepp; '
                               'Run: "%(prog) list repos" for avail repos')

    equiv.add_argument('name',
                       help='Package name')

    equiv.add_argument('--source-repo',
                       help='Optionally define the source repo name, '
                            'Run: "%(prog) list repos" for avail repos')

    equiv.add_argument('--repos',
                       type=split_args,
                       help='Limit to specific repositories comma , sep; '
                            'Run: "%(prog) list repos" for avail repos')

    return main_parser


def main():

    args = build_args()
    opts = args.parse_args()

    output_init(opts.output)

    if opts.command == 'list':
        list(opts)


def list(opts):

    if opts.what == 'providers':
        output([x for x in all_providers.keys()])


def output_init(format):
    global output

    def _output(data):

        if format == 'text':
            print(data)

        elif format == 'json':
            print(json.dumps(data))
        else:
            raise ValueError(f'{format} is not supported')

    output = _output
