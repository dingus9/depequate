import importlib
import os
import pkgutil


all_providers = {x.name: importlib.import_module(f'depequate.providers.{x.name}')
                 for x in
                 pkgutil.iter_modules([os.path.dirname(__file__)])}
